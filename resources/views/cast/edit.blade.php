@extends('layout.master')
@section('judul')
Halaman Edit Cast Film
@endsection 

@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
  @csrf
  @method('put')
    <div class="mb-3">
      <label>Nama Cast</label>
      <input type="text" value="{{ $cast->nama }}" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
      <label>Umur Cast</label>
      <input type="text" value="{{ $cast->umur }}" class="form-control" name="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
      <label>Bio Cast</label>
      <input type="text" value="{{ $cast->bio }}" class="form-control" name="bio">
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection