@extends('layout.master')
@section('judul')
Halaman Detail Cast Film
@endsection 

@section('content')

<h1> {{ $cast->nama }} </h1>
<p> {{ $cast->umur }} </p>
<p> {{ $cast->bio }} </p>
<div class="row">
@forelse ($cast->post as $item)
<div class="col-4">
    <div class="card">
      <img src="{{ asset('gambar/'. $item->gambar) }}" class="card-img-top mt-3" alt="...">
      <div class="card-body">
        <h3>{{ $item->title }}</h3>
        <p class="card-text">{{ Str::limit($item->body,20) }}</p>
       
        <a href="/post/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
        </div>
    </div>
</div>
@empty
    <h1>Tidak Post Di Cast Ini</h1>
@endforelse
</div>
@endsection