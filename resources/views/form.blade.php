<html>

<body>
    <h1>Buat akun baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" action="post">
        @csrf
        <label>First Name : </label><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name : </label><br>
        <input type="text" name="last_name"><br><br>

        <label>Gender</label><br><br>
        <input type="radio" name="male">Male<br>
        <input type="radio" name="female">Female<br><br>

        <label>Nationality</label><br><br>
        <select name="nation" id="nation">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Other">Other</option>
        </select><br><br>

        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="english">English<br>
        <input type="checkbox" name="other">other<br><br>

        <label>Bio</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="kirim">
    </form>


</body>

</html>