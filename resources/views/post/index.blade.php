@extends('layout.master')
@section('judul')
Halaman Post
@endsection 

@section('content')
@auth
    

<a href="/post/create/" class="btn btn-success">Tambah Cast</a>
@endauth
    <div class="row">
        
          @forelse ($post as $key=>$item)
        <div class="col-4">
          <div class="card">
            <img src="{{ asset('gambar/'. $item->gambar) }}" class="card-img-top mt-3" alt="...">
            <div class="card-body">
              <h3>{{ $item->title }}</h3>
              <span class="badge badge-success">{{ $item->cast->nama }}</span>
              <p class="card-text">{{ Str::limit($item->body,20) }}</p>
              <p class="card-text">Cast Oleh : {{ $item->cast_id}}</p>
              @auth
                  
              
              <form action="/post/{{  $item->id }}" method="POST">
                @csrf
                @method('delete')
                
                <a href="/post/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
              </form>
              @endauth

              
              <a href="/post/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
              
            </div>
            
          </div>
        </div>
                     
               
          @empty
              <h4>DATA KOSONG</h4>
          @endforelse
        
    </div>     
@endsection