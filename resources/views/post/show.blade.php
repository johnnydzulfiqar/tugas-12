@extends('layout.master')
@section('judul')
Halaman Post
@endsection 

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
            <img src="{{ asset('gambar/'. $post->gambar) }}" width="480px" height="240px" alt="...">
            <div class="card-body">
              <h3>{{ $post->title }}</h3>
              <p class="card-text">{{ $post->body }}</p>
              
             <a href="/post" class="btn btn-info btn-sm">Kembali</a>
            </div>
        </div>
        <h1>List Kritik</h1>
        @forelse ($post->kritik as $item)
        <div class="card" style="width: 18rem;">
            
            <div class="card-body">
              <h1 class="card title">{{ $item->user->name }}</h1>
              <h4>Rating = {{ $item->point }}/10</h2>
              <p class="card-text">{{ $item->content }}</p>
              
            </div>
          </div>
        @empty
            <h1>Belum ada Kritik</h1>
        @endforelse
    </div>
  
        
    
    
    
</div>  
@auth
    <form action="/kritik" method="POST" class="mt-2">
        @csrf
        <input type="hidden" value="{{ $post->id }}" name="post_id">
        <div class="mb-3">
            <label>Content</label>
            <input type="text" class="form-control" name="content">
          </div>
          @error('content')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="mb-3 ml-3">
            <label>Point</label>
            <input type="number" class="form-control" name="point">
          </div>
          @error('point')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <input type="submit" value="tambah">
    </form>
    @endauth
@endsection