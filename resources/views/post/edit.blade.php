@extends('layout.master')
@section('judul')
Halaman Tambah Post
@endsection 

@section('content')

<h2>Edit Data</h2>
        <form action="/post/{{ $post->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" value={{ $post->title }} class="form-control" name="title" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">body</label>
                <textarea name="body" class="form-control">{{ $post->body }} </textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Cast</label>
                <select name="cast_id" class="form-control">
                    <option value="">Pilih Cast</option>
                    @foreach ($cast as $item)
                        @if ($item->id === $post->cast_id)
                        <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                        @else
                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endif
                        
                    @endforeach
                </select>
                @error('cast_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="gambar">Gambar</label>
                    <input type="file" class="form-control" name="gambar">
                    
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
@endsection