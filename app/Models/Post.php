<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "post";
    protected $fillable = ["title", "body", "cast_id", "gambar"];


    public function cast()
    {
        return $this->belongsTo(Cast::class, 'cast_id');
    }

    public function kritik()
    {
        return $this->hasMany(Kritik::class);
    }
}
