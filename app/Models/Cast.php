<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = "cast";
    protected $fillable = ["nama", "umur", "bio"];


    public function post()
    {
        return $this->hasMany(Post::class, 'cast_id');
    }
}
