<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {

        $profile = Profile::where('users_id', Auth::id())->first();

        return view('profile.edit', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate(
            [
                'umur' => 'required',
                'bio' => 'required',
                'alamat' => 'required',
            ],
            [
                'umur.required' => 'Tolong isi ',
                'bio.required' => 'Tolong isi',
                'alamat.required' => 'Tolong ',
            ]
        );
        $profile = Profile::find($id);
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;

        $profile->save();

        return redirect('/profile');
    }
}
