<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\ViewErrorBag;

class HomeController extends Controller
{
    public function dashboard()
    {
        return View('kosong');
    }

    public function table()
    {
        return View('index');
    }

    public function data()
    {
        return View('data-table');
    }
}
