<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate(
            [
                'content' => 'required',
                'point' => 'required',

            ],

        );
        $kritik =  new Kritik;
        $kritik->content = $request->content;
        $kritik->point = $request->point;
        $kritik->post_id = $request->post_id;
        $kritik->users_id = Auth::id();
        $kritik->save();

        return redirect('/post/' . $request->post_id);
    }
}
