<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return View('form');
    }

    public function welcome(Request $request)
    {
        // dd($request->all());
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        return View('welcome2', compact('first_name', 'last_name'));
    }
}
