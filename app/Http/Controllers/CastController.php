<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cast;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Tolong isi nama cast',
                'umur.required' => 'Tolong isi umur cast',
                'bio.required' => 'Tolong isi bio cast',
            ]
        );
        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
        );
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.index', ['cast' => $cast]);
    }

    public function show($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);

        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')->where('id', $cast_id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update($cast_id, Request $request)
    {
        $validated = $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Tolong isi nama cast',
                'umur.required' => 'Tolong isi umur cast',
                'bio.required' => 'Tolong isi bio cast',
            ]
        );
        DB::table('cast')->where('id', $cast_id)->update(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
        );
        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        DB::table('cast')->where('id', '=', $cast_id)->delete();
        return redirect('/cast');
    }
}
