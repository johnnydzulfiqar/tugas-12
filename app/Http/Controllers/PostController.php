<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Cast;

use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    public function __construct()
    {
        //buat session keculai index
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
        return view('post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = Cast::all();
        return view('post.create', compact('cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'title' => 'required',
                'body' => 'required',
                'cast_id' => 'required',
                'gambar' => 'required | mimes:jpeg,jpg,png|max:2200',
            ],
            [
                'title.required' => 'Input bro',
                'body.required' => 'Input bro',
                'cast_id.required' => 'Input bro',
                'gambar.required' => 'Input bro',
            ]
        );
        $gambar = $request->gambar;
        $new_gambar = time() . '-' . $gambar->getClientOriginalName();

        $post =  new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->cast_id = $request->cast_id;
        $post->gambar = $new_gambar;
        $post->save();
        $gambar->move('gambar/', $new_gambar);

        return redirect('/post');
        Post::create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrfail($id);
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrfail($id);
        $cast = Cast::all();
        return view('post.edit', compact('post', 'cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'title' => 'required',
                'body' => 'required',
                'cast_id' => 'required',
                'gambar' => 'mimes:jpeg,jpg,png|max:2200',
            ],
            [
                'title.required' => 'Input bro',
                'body.required' => 'Input bro',
                'cast_id.required' => 'Input bro',
                'gambar.mimes' => 'Format Tidak sesuai'
            ]


        );
        $post = Post::find($id);
        if ($request->has('gambar')) {
            $path = "gambar/";
            File::delete($path . $post->gambar);
            $gambar = $request->gambar;
            $new_gambar = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('gambar/', $new_gambar);

            $post->gambar = $new_gambar;
        }
        $post->title = $request->title;
        $post->body = $request->body;
        $post->cast_id = $request->cast_id;

        $post->save();
        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $path = "gambar/";
        File::delete($path . $post->gambar);
        $post->delete();

        return redirect('/post');
    }
}
