<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);

Route::get('/table', [HomeController::class, 'table']);

Route::get('/data-table', [HomeController::class, 'data']);

//middleware or session
Route::group(['middleware' => ['auth']], function () {
    //new Crud cast create
    Route::get('/cast/create', 'App\Http\Controllers\CastController@create');
    //menyimpan
    Route::post('/cast', 'App\Http\Controllers\CastController@store');
    //cast index data
    Route::get('/cast', 'App\Http\Controllers\CastController@index');
    //detail cast
    Route::get('/cast/{cast_id}', 'App\Http\Controllers\CastController@show');
    //edit cast
    Route::get('/cast/{cast_id}/edit', 'App\Http\Controllers\CastController@edit');
    //update cast
    Route::put('/cast/{cast_id}', 'App\Http\Controllers\CastController@update');
    //delete
    Route::delete('/cast/{cast_id}', 'App\Http\Controllers\CastController@destroy');
    //profile
    Route::resource('profile', 'App\Http\Controllers\ProfileController')->only(['index', 'update']);
    //kritik
    Route::resource('kritik', 'App\Http\Controllers\KritikController')->only(['store']);
});

//route Resources

Route::resource('post', 'App\Http\Controllers\PostController');
Auth::routes();

    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
